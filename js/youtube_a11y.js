// ayt global namespace
var ayt = (function (window, document, $, swfobject, undefined) {
  var my = {};
  var settings = {
    params : { allowScriptAccess: "always" }
  };

  my.getVideos = function($body){
    return $body.find('.ytplayer');
  };
  my.embedVideos = function(videos){
    videos.each(function(index){
      var id = $(this).attr('data-yt-id'),
          width = $(this).attr('data-yt-width'),
          height = $(this).attr('data-yt-height');

      swfobject.embedSWF(
      "http://www.youtube.com/v/" + id + "&enablejsapi=1&playerapiid=ytplayer", "ytplayer-" + id, width, height, "8", null, null, settings.params);
    });
  };
  my.toggleControls = function($body){
    var $controls = $body.find('.accessible-controls-wrap'),
        $controlToggle = $controls.find('.accessible-controls-toggle'),
        $controlList = $controls.find('.accessible-controls');

    $controlList.hide();

    $controlToggle.click(function(e){
      e.preventDefault();
      $(this).next().toggle();
    });
  };
  my.control = function(action, id){
    var video = document.getElementById('ytplayer-' + id),
        allVideos = document.getElementsByTagName('object');

    if(video){
      switch(action){
        case 'play':
          // pause other running youtube videos
          $.each(allVideos, function(index, value){
            if(typeof value.pauseVideo === 'function'){
              value.pauseVideo();
            }
          });
          video.playVideo();
          break;
        case 'pause':
          video.pauseVideo();
          break;
        case 'stop':
          video.stopVideo();
          break;
        default:
          alert('Oops, something went wrong. The video wasnt found');
      }
    }
  };
  my.init = function (){
    $(document).ready(function(){
      var $body = $('body');
      var $videos = my.getVideos($body);

      my.embedVideos($videos);
      my.toggleControls($body);
    });
  };

  return my;
})(window, document, jQuery, swfobject);

// initialize
ayt.init();